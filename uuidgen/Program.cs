﻿using System;

namespace uuidgen
{
    class Program
    {
        static void Main(string[] args)
        {
            Guid guid = Guid.NewGuid();
            Console.WriteLine(guid.ToString());
        }
    }
}
